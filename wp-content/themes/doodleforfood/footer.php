			</main>
			
			<footer class="page">
				<nav class="page clearfix">
					<div class="inner-wrap clearfix">
						<ul class="social clearfix">
							<li class="follow">Follow Doodle for Food:</li>
						<?php
							$frontID = get_option('page_on_front');
						# get the contact email address	
							$contact_email = get_field('contact_e-mail_address', $frontID);
							
						# get the social media buttons
							$social_buttons = get_field('social_media_buttons', $frontID);
							$social_buttons_field = get_field_object('field_57c4f94922909');
							$social_choices = $social_buttons_field['choices'];
							
							if(is_array($social_buttons)){
								foreach($social_buttons as $btn):
								# spit out the values
									$url = $btn['url'];
									$serviceabbr = $btn['service'];
									$servicename = $social_buttons_field['choices'][$btn['service']];
						?>
							<li><a href="<?php echo $url; ?>" class="hvr-grow-rotate <?php echo $serviceabbr; ?>" title="<?php echo $servicename; ?>" target="_blank"></a>
						<?php
								endforeach;
							}
						
						?>
							<li><a href="<?php bloginfo('rss2_url'); ?>" class="hvr-grow-rotate rss" title="RSS" target="_blank"></a>
						</ul>
						<ul class="copyr clearfix">
							<li>&copy; <?php echo date('Y'); ?> by Megan McKay
						<?php if ($contact_email): ?>
							<li><a href="mailto:<?php echo $contact_email; ?>">Contact</a>
						<?php endif; ?>
						</ul>
					</div>
				</nav>
			</footer>

	  </div><!-- eo .wrap -->

		<script>window.jQuery || document.write('<script src="<?php bloginfo('stylesheet_directory'); ?>/js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
		
		<?php wp_footer(); ?>
		
		<script>
		(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
		function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
		e=o.createElement(i);r=o.getElementsByTagName(i)[0];
		e.src='https://www.google-analytics.com/analytics.js';
		r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
		ga('create','UA-XXXXX-X','auto');ga('send','pageview');
		</script>
	</body>
</html>