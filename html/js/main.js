(function($) {

$(window).load(function(){
	
// hide the mobile menu
	if($('.menu').is(':visible')){
		$('header.page nav .inner-wrap').hide();
		$('header.page').trigger("sticky_kit:detach");
		//$('.mark').hide();
	} else {
		doStickyKit();
	}
// mobile menu functionality
	$('.menu').click(function(e){
		e.preventDefault();
		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$(this).next().slideUp();
		} else {
			$(this).addClass('active');
			$(this).next().slideDown();
		}
	});
	
// hide the share menu
	if($('.share .btns').is(':visible')){
		$(this).hide();
		$('.share').removeClass('active');
	}
	
	$('.share').click(function(e){
		e.preventDefault();
		// check if the click is on a share button
		if( $(e.target).attr('class') ){}
		else
		// clicking on the share button smooth animates them in
		if($('.share .btns').is(':visible')){
			$('.share .btns').slideUp();			
			// if desktop, slowly animate the button down
			if($('.mobile.menu').is(':hidden')){
				$('.share').animate({ top: '+=20' }, 400,function(){
					$('.share').removeAttr('style').removeClass('active');
				});
			} else {
			// otherwise, just do the normal behaviour
				$('.share').removeClass('active');
			}
		} else {
			$('.share .btns').hide().slideDown();
			// if desktop, slowly animate the button up
			if($('.mobile.menu').is(':hidden')){
				$('.share').animate({ top: '-=20' }, 400,function(){
					$('.share').removeAttr('style').addClass('active');
				});
			} else {
			// otherwise, just do the normal behaviour
				$(this).addClass('active');
			}
		}
		
	})
	
	
}).resize(function(){
// if the window is resized, reset the menu state
	$('.menu').removeClass('active');
	if($('.menu').is(':visible')) {
		$('header.page nav .inner-wrap').hide();
		$('header.page').trigger("sticky_kit:detach");
		$('.mark').show();
	} else {
		$('nav.page .inner-wrap, .mark').show();
		doStickyKit();
	}
// reset the share menu state
	if($('.share .btns').is(':visible')){
		$('.share .btns').hide();
		$('.share').removeClass('active');
	}


});

function doStickyKit(){
	$('header.page').stick_in_parent({offset_top: -112})
		.on("sticky_kit:stick", function(e) {
	    $('.mark').hide();
	  })
	  .on("sticky_kit:unstick", function(e) {
	    $('.mark').show();
	  });
}

})( jQuery );