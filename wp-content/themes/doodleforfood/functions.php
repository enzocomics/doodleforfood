<?php
###################################################
# Enqueue Styles & Scripts

add_action('init', 'jquery_in_footer');
	function jquery_in_footer() {
		if (!is_admin()) {
			// Comment out the next two lines to load the local copy of jQuery
			wp_deregister_script('jquery'); 
			wp_register_script('jquery', 'https://code.jquery.com/jquery-1.12.0.min.js', false, '1.12.0', true); 
			wp_enqueue_script('jquery');
		}
	}
	
add_action('wp_enqueue_scripts', 'queue_scripts');
	
	function queue_scripts(){
		global $wp_styles;
		global $wp_scripts;

		wp_enqueue_style( 'font', 									'https://fonts.googleapis.com/css?family=Montserrat:400,700|Open+Sans:400,400italic,700,700italic' );
		wp_enqueue_style( 'normalize',							get_bloginfo('stylesheet_directory').'/css/normalize.css' );
		wp_enqueue_style( 'primary',								get_bloginfo('stylesheet_directory').'/style.css', array('normalize') );
		wp_enqueue_script( 'modernizr', 						get_bloginfo('stylesheet_directory').'/js/vendor/modernizr-2.8.3.min.js', '', '2.8.3' );
		
		wp_enqueue_script( 'plugins',								get_bloginfo('stylesheet_directory').'/js/plugins.js', '', '', true );
		wp_enqueue_script( 'stickykit',							get_bloginfo('stylesheet_directory').'/js/vendor/stickykit-1.1.2.min.js', '', '1.1.2', true );
		wp_enqueue_script( 'main',									get_bloginfo('stylesheet_directory').'/js/main.js?v=2.0', '', '', true );
				
	}
	
	
###################################################
# Remove extra wp_head junk

	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator'); 
	
# add theme support
	add_theme_support('menus');
	add_theme_support('post-thumbnails'); 
	add_theme_support('title-tag' ); // let wordpress handle the title tag

	register_nav_menus(
		array( 'header-nav' => __( 'Main Navigation' ) )
	);
	


###################################################
# Create Comics Post Type

add_action('init', 'create_comics_post_type');

function create_comics_post_type(){
	register_post_type( 'comic',
		array(
			'labels' => array(
				'name' 			=> __('Comics'),
				'singular_name' => __('Comic'),
				'add_new_item'	=> __('Add New Comic'),
				'edit_item'		=> __('Edit Comic'),
				'new_item'		=> __('New Comic'),
				'view_item'		=> __('View Comic'),
				'view_items'	=> __('View Comics'),
				'search_items'	=> __('Search Comics'),
			),
			'public'		=> true,
			'has_archive'	=> true,
			'show_in_menu'	=> true,
			'menu_position' => 5,
			'menu_icon'		=> 'dashicons-grid-view',
			'taxonomies' => array(
				'post_tag'
			)
		)
	);
}



###################################################
# Random Button Functions

function random_add_rewrite() {
	global $wp;
	$wp->add_query_var('random');
	add_rewrite_rule('random/?$', 'index.php?random=1', 'top');
	flush_rewrite_rules();
}

function random_template() {
	if (get_query_var('random') == 1) {
		$posts = get_posts('post_type=comic&orderby=rand&numberposts=1');
		$link = get_permalink($posts[0]->ID);
		wp_redirect($link,307);
		exit;
	}
}


# add random button
	add_action( 'init','random_add_rewrite' );
	add_action( 'template_redirect','random_template' );
