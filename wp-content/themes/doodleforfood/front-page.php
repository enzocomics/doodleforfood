<?php get_header(); ?>
				<?php get_template_part('comic'); ?>
				
				<div class="content row clearfix">
					<div class="blogroll main-col">
					<?php
					wp_reset_postdata();
						$mewargs = array(
							'numberposts'	=> 1,
							'orderby'	 	=> 'date',
							'post_type'		=> 'post',
							'post_status'	=> 'publish',
							'order'			=> 'DESC'
						);
						$newuery = new WP_Query($mewargs);
					
					?>
					<?php if ( $newuery->have_posts() ) : while ( $newuery->have_posts() ) : $newuery->the_post(); ?>

						<article>
							<h1 class="title"><?php the_title(); ?></h1>
							<span class="subtitle"><?php the_date(); ?></span>
							<?php the_content(); ?>
						</article>
					<?php endwhile;  wp_reset_postdata(); else: endif; ?>
						
					</div><!-- eo .blogroll -->
					
					<aside>
						<div class="medium-rectangle ad"></div>
					</aside>
				</div><!-- eo .content -->
<?php get_footer(); ?>