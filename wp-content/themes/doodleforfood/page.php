<?php get_header(); ?>			
				<div class="content row clearfix">
					<div class="main-col">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<article>
							<h1 class="title"><?php the_title(); ?></h1>
							<?php the_content(); ?>
						
						</article>
					<?php endwhile;  else: endif; ?>
						
						
					</div><!-- eo .blogroll -->
					
					<aside>
						<div class="medium-rectangle ad"></div>
					</aside>
				</div><!-- eo .content -->
<?php get_footer(); ?>