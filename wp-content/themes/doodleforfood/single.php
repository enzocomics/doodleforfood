<?php get_header(); ?>
				<?php get_template_part('comic'); ?>
				
				<div class="content row clearfix">
					<div class="blogroll main-col">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<article>
							<h1 class="title"><?php the_title();?></h1>
							<span class="subtitle"><?php the_date(); ?></span>
							<?php the_content(); ?>
						</article>
					<?php endwhile;  else: endif; ?>
						
					</div><!-- eo .blogroll -->
					
					<aside>
						<div class="medium-rectangle ad"></div>
					</aside>
				</div><!-- eo .content -->
<?php get_footer(); ?>