<!doctype html>
<!-- 
======================================================================
______                _ _         __            ______              _ 
|  _  \              | | |       / _|           |  ___|            | |
| | | |___   ___   __| | | ___  | |_ ___  _ __  | |_ ___   ___   __| |
| | | / _ \ / _ \ / _` | |/ _ \ |  _/ _ \| '__| |  _/ _ \ / _ \ / _` |
| |/ / (_) | (_) | (_| | |  __/ | || (_) | |    | || (_) | (_) | (_| |
|___/ \___/ \___/ \__,_|_|\___| |_| \___/|_|    \_| \___/ \___/ \__,_|

======================================================================

© 2016 by Megan McKay. All rights reserved.

Powered by WordPress, Easel, and seventeen electric generators run by
a team of unitard-wearing sloths on stationary bicycles

Site built by EnzoComics. hmu bb enzo@cheerupemokid.com | http://twitter.com/enzocomics

-->
<?php
	$styledir = get_bloginfo('stylesheet_directory');
?>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php echo $styledir; ?>/img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php bloginfo('stylesheet_directory'); ?>/img/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="76x76"   href="<?php echo $styledir; ?>/img/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo $styledir; ?>/img/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo $styledir; ?>/img/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo $styledir; ?>/img/apple-touch-icon-180x180.png">
		<link rel="icon" sizes="192x192" href="<?php echo $styledir; ?>/img/icon-hires.png"/>
		<link rel="icon" sizes="128x128" href="<?php echo $styledir; ?>/img/icon-normal.png"/>
		<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php endif; ?>
<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<!--[if lt IE 8]>
	   	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
		<div class="wrap">
			<header class="page">
				<div class="top clearfix">
					<a href="<?php bloginfo('home'); ?>" id="logo" class="logo hvr-grow-rotate" >
						<img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-mark.png" class="mark desktop" alt="Doodle for Food" width="368" height="117" /><!--
				 --><img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-wordmark.png" class="wordmark"alt="Doodle for Food" width="368" height="106" />
					</a>
					<div class="leaderboard ad desktop"></div>
				</div>
					<nav class="page clearfix">
					<a href="#" class="mobile menu-btn"></a>
					<div class="inner-wrap clearfix">
						<?php
						# get the wordpress menu
							wp_nav_menu( array(
							 'menu' => 'Main Navigation',
							 'container' => null,
							 'depth' => 1,
							 'items_wrap' => '<ul id="%1$s" class="main clearfix %2$s">%3$s</ul>'
							));
							
						# get the social media buttons
							$frontID = get_option('page_on_front');
							$social_buttons = get_field('social_media_buttons', $frontID);
							#print_r($social_buttons);
							$social_buttons_field = get_field_object('field_57c4f94922909');
							$social_choices = $social_buttons_field['choices'];
						?>
						<ul class="social">
						<?php
						if(is_array($social_buttons)){
							foreach($social_buttons as $btn):
							# spit out the values
								$url = $btn['url'];
								$serviceabbr = $btn['service'];
								$servicename = $social_buttons_field['choices'][$btn['service']];
						?>
							<li><a href="<?php echo $url; ?>" class="hvr-grow-rotate <?php echo $serviceabbr; ?>" title="<?php echo $servicename; ?>" target="_blank"></a>
						<?php
							endforeach;
						}
						
						?>
							<li><a href="<?php bloginfo('rss2_url'); ?>" class="hvr-grow-rotate rss" title="RSS" target="_blank"></a>
						</ul>
					</div><!-- eo .inner-wrap -->
				</nav>
			</header>

			<main>
		 