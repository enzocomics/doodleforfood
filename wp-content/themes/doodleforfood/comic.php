<?php
	wp_reset_query();
	global $wp_query, $post;
	if(is_single() && ($post->post_type == 'comic')):
		displayStrip();
	elseif( (is_home() || is_front_page()) && !is_paged() ):
		$comic_args = array(
				'showposts' => 1,
				'posts_per_page' => 1,
				'post_type' => 'comic'
				);
		$wp_query->in_the_loop = true;
        $comicFrontpage = new WP_Query();
        $comicFrontpage->query($comic_args);
		while ($comicFrontpage->have_posts()) : $comicFrontpage->the_post();
			displayStrip(get_the_ID());
			break;
		endwhile;
		#ceo_UnProtect();
	
	elseif( is_search() ):
	endif;

	# display an edited comic navigation
		
    function displayStripNav(){
        global $post, $wp_query;

            $first_comic =	getFirstComic();
            $last_comic = 	getLastComic();
            $next_comic = 	getNextComic();
            $prev_comic = 	getPrevComic();		
    
        $first_btn		= (get_permalink() !== $first_comic) ? '<a href="'.$first_comic.'" class="hvr-grow-rotate btn first" title="First Strip""></a>' : '<span class="first inactive"></span>';
        $last_btn			= (get_permalink() !== $last_comic && !is_front_page())	? '<a href="'.$last_comic.'" class="hvr-grow-rotate btn last" title="Latest Strip""></a>' : '<span class="last inactive"></span>';
        $prev_btn			=	(get_permalink() !== $prev_comic) ? '<a href="'.$prev_comic.'" class="hvr-grow-rotate btn prev" title="Previous Strip"></a>' : '<span class="prev inactive"></span>';
        $next_btn			=	(get_permalink() !== $next_comic) ? '<a href="'.$next_comic.'" class="hvr-grow-rotate btn next" title="Next Strip""></a>' : '<span class="next inactive"></span>';
        $random_btn 	= '<a href="'.get_bloginfo('url').'/random" class="hvr-grow-rotate btn random" title="Random Strip"></a>' /*: '<span class="random inactive"></span>'*/;
    
        echo 
            '<li>'.$first_btn.'</li>'.
            '<li>'.$prev_btn.'</li>'.
            '<li>'.$random_btn.'</li>'.
            '<li>'.$next_btn.'</li>'.
            '<li>'.$last_btn.'</li>'
        ;		
    }


	function displayStrip(){
		global $wp_query, $post;
		
	# set up the social sharing links
        $tumblrlink     = "https://www.tumblr.com/widgets/share/tool?canonicalUrl=".get_permalink()."&posttype=link";
		$fblink			= "http://www.facebook.com/sharer/sharer.php?u=".get_permalink();
		$redditlink		= "http://www.reddit.com/r/comics/submit?url=".get_permalink()."&amp;title=".get_the_title()."%20-%20".get_bloginfo('name');
		$pinterestlink	= "http://pinterest.com/pin/create/button/?url=".get_permalink()."&media={URI-encoded URL of the image to pin}&description={optional URI-encoded description}";
		$tweetlink 		= "http://twitter.com/intent/tweet?original_referer=" . get_bloginfo('siteurl') ."&text=%22" . get_the_title() . "%22%20-%20". get_bloginfo('name') . "&url=". get_the_permalink() . "&via=doodleforfood";
		$emaillink		= "mailto:?subject=A%20friend%20shared%20a%20comic%20with%20you%20from%20%22".get_bloginfo('name')."%22&amp;body=Hey!%20I%20thought%20you%20would%20enjoy%20this%20comic:%20%22".get_the_title()."%22%20%3C".get_permalink()."%3E";
?>

				<div id="comic" class="comic content">
					<nav class="top row">
						<ul>
							<?php displayStripNav(); ?>
						</ul>
					</nav>
					<div class="display row">
						<?php
						# display the comic images
							$comic_images = get_field("comic_images" ) ;		
							if(is_array($comic_images)){
								foreach($comic_images as $panel){
									$url = $panel['comic_image']['url'];
									echo '<img src="'.$url.'" alt="" />';
									
								}
							}
						# or else, no comic image is displayed
						?>
					</div>
					<nav class="bottom row">
						<ul class="clearfix">
							<?php displayStripNav(); ?>
					 		<li class="text comments"><a href="<?php the_permalink(); ?>">Comments (10)</a>
							<li class="text share active"><a href="#">Share this comic!</a>
								<ul class="btns">
									<li><a href="<?php echo $tumblrlink; ?>" class="tm hvr-grow-rotate" title="Tumblr"></a>
									<li><a href="<?php echo $tweetlink; ?>" class="tt hvr-grow-rotate" title="Twitter"></a>
									<li><a href="<?php echo $fblink; ?>" class="fb hvr-grow-rotate" title="Facebook"></a>
									<li><a href="<?php echo $pinterestlink; ?>" class="pi hvr-grow-rotate" title="Pinterest"></a>
									<li><a href="<?php echo $redditlink; ?>" class="rd hvr-grow-rotate" title="Reddit"></a>
									<li><a href="<?php echo $emaillink; ?>" class="em hvr-grow-rotate" title="Email"></a>
								</ul>
						</ul>
					</nav>
				</div><!-- eo #comic -->
<?php 
		}
		
###################################################
# Comic Navigation Functions

function getFirstComic(){
	$args = array(
		'numberposts'	=> 1,
		'orderby'	 	=> 'date',
		'post_type'		=> 'comic',
		'post_status'	=> 'publish',
		'order'			=> 'ASC'
	);
	$comic = get_posts($args);
	$comicID = $comic[0]->ID;
	return get_post_permalink($comicID);
}

function getLastComic(){
	$args = array(
		'numberposts'	=> 1,
		'orderby'	 	=> 'date',
		'post_type'		=> 'comic',
		'post_status'	=> 'publish',
		'order'			=> 'DESC'
	);
	$comic = get_posts($args);
	$comicID = $comic[0]->ID;
	return get_post_permalink($comicID);
}

function getNextComic(){
	$comic = get_next_post();
	$comicID = $comic->ID;
	return get_post_permalink($comicID);
}

function getPrevComic(){
	$comic = get_previous_post();
	$comicID = $comic->ID;
	return get_post_permalink($comicID);
}
		
?>